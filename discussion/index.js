// Array Methods- Javascript has built-in functions and methods for arrays. This allows us to manipulate and access aray items.

// Mutator Methods
/*
	-Mutator methods are functions that "mutate" or change an array after they're created.
	-These methods manipulate the original array performing various task such as adding and removing elements.
*/

// push ()
/*
	-Adds an element in the end of an array and return the array's length.


	Syntax:
		arrayName.push();
*/


let fruits = ['Apple', 'Orange', 'Kiwi', 'Daragon Fruit'];

console.log('Current array');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
// console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);


// pop()
/*
	-Remove the last element in an array and returns the removed element.
	-Syntax
		arrayName.pop();
*/


let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);


// Unshift()
/*
	-Adds one or more elements at the beginning of an array.

	Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', elementB);
*/
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);



// Shift()
/*
	-Removes an element at the beginning of an array and Returns the removed elements

	Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);


// Splice()
/*
	-Simultaneously removes elements from a specified index number and adds elements. or remove/add

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/

fruits.splice(1, 0, 'Lime', 'Cherry', 'Lemon');
console.log('Mutated array from splice method:');
console.log(fruits);


// sort()
/*
	-Rearange the array elemets in alphanumeric order.

	Syntax:
		arrayName.sort();
*/
const randomThings = ["cat", "boy", "apps", "zoo"]

randomThings.sort();
console.log('Mutated array form sort method');
console.log(randomThings);


// Reverse()
/*
	-Reverse the order of array elements

	Syntax:
		arrayName.reverse();
*/
randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);


// Non-Mutator Methods
/*
	Non-Mutator methods are functions that do not modify or change an array after they're created.
	-These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing output.
*/

// indexOf()
/*
	-Ruturns the index number of the first matching element found in an array.
	-If no match was found, the result will be
	-1
	-The search process will be done from first element proceeding to the last elements.

	Syntax:
		arrayName.indexOf(searchValue)
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']

let firstIndex = countries.indexOf('CAN');
console.log('Result of indexOf method:' + firstIndex);


// Slice()
/*
	-Portion/slices elements from an array AND returns a new array.

	Syntax:
		arrayName.slice(startingindex);
		arrayName.slice(startingindex, endingIndex);
*/

// Slicing off elements from a specified index to the first element.

let slicedArrayA = countries.slice(4);
console.log('Result from slice A method:');
// console.log(countries);
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index

// Note: the last element is not included.
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice B method:');
console.log(slicedArrayB);

// Slicing off Elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log('Result form the slice C method');
console.log(slicedArrayC);


// forEach();
/*
	-Similar to a for loop that iterates on each array element.
	-For each item in the array, the anonymous function passed in forEach() method will be run.
	-arrayName -plural for good practice
	-parameter - singular
	-Note: it must have function.
	
	Syntax:
		arrayName.forEach(function(indivElement) {
			statement
		})
*/

countries.forEach(function(country) {
	console.log(country);
});


// includes()
/*
	includes() 

	-includes() method checks if the argument passed can be found in the array.
	-it returns a boolean which can be saved in a variable.
		-return if the argument is found in the array.
		-return false if it is not.

		Syntax:
			arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false